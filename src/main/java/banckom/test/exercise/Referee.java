package banckom.test.exercise;

public class Referee implements IReferee {
    private Playground playground;
    // Singleton instance
    private static Referee REFEREE = null;

    private Referee(Playground playground) {
        this.playground = playground;
    }

    public synchronized static Referee getInstance(Playground playground) {
        if (REFEREE == null) {
            REFEREE = new Referee(playground);
        }
        return REFEREE;
    }

    public boolean checkPlayerMovementForFoul(Player player, Movement direction) {
        // TODO: implement logic
        throw new UnsupportedOperationException();
    }

    public boolean checkIfPlayerMoveIsAllowed(Player player, Movement direction) {
        // TODO: implement logic
        throw new UnsupportedOperationException();
    }

    public void ejectPlayerFromTheGame(Player player) {
        playground.evictPlayer(player);
    }
    // Return player to the game after first ejection.
    public void returnPlayerToTheGame(Player player) {
        playground.registerPlayer(player);
        player.ready();
    }

}

package banckom.test.exercise;

import org.apache.commons.lang.math.RandomUtils;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Playground implements Runnable {
    private static final Logger logger = Logger.getLogger(Playground.class.getName());
    private Referee referee = null;
    public static final int width = 100;
    public static final int height = 100;


    private Cell[][] playgroundCells = new Cell[width][height];

    // Singleton pattern
    private Playground() {
        initializePlaygroundCells();
    }
    private static Playground PLAYGROUND = new Playground();

    public static Playground getInstance() {
        return PLAYGROUND;
    }

    // Currently registered players. Only registered players will be able to make movements.
    private Set<Player> registeredPlayers = new HashSet<Player>();

    // Blocking queue to hold on to the movement requests. Serve the request in FIFO manner.
    private BlockingQueue<PlayerMovement> queue = new ArrayBlockingQueue<PlayerMovement>(100, true);

    public void requestMove(PlayerMovement movement) {
        try {
            if(registeredPlayers.contains(movement.getPlayer()))
                queue.put(movement);
        } catch (InterruptedException ex) {
            throw new IllegalStateException("Could not put new request", ex);
        }
    }

    public void run() {
        while (true) {
            try {
                PlayerMovement moveRequest = queue.take();
                Player player = moveRequest.getPlayer();
                Movement movement = moveRequest.getMovement();
                if (referee.checkIfPlayerMoveIsAllowed(player, movement)) {
                    boolean moveIsFouled = referee.checkPlayerMovementForFoul(player, movement);
                    if (moveIsFouled) {
                        player.yellowCard();
                        if(player.isNeedToBeEjected()){
                            referee.ejectPlayerFromTheGame(player);
                        }
                    }
                    movePlayer(player, movement);
                    if (registeredPlayers.contains(player) == false && moveIsFouled == false) {
                        registeredPlayers.add(player);
                    }
                }
                // Check for winner.
                if (registeredPlayers.size() == 1) {
                    winner();
                    break;
                }
            } catch (InterruptedException ex) {
                throw new IllegalStateException("Could not process requests.", ex);
            }
        }
    }

    private final void movePlayer(Player player, Movement movement) {
        Cell newCell = player.getCell().getCellInDirection(movement);
        newCell.setPlayer(player);
        player.getCell().setPlayer(null);
        player.setCell(newCell);
    }

    private final void initializePlaygroundCells() {
        for (int row = 0; row < height; row++) {
            for (int column = 0; column < width; column++) {
                Cell cell = new Cell(row, column, this);
                playgroundCells[row][column] = cell;
            }
        }
    }

    private void winner() {
        Player winningPlayer = registeredPlayers.iterator().next();
        logger.log(Level.INFO, "{0} won!", winningPlayer);
        System.exit(0);
    }
    // Tries to place a player to a randomly selected cell. Recursively calls itself until it finds an empty cell.
    public void registerPlayer(Player player) {
        int row = RandomUtils.nextInt(height);
        int column = RandomUtils.nextInt(width);

        Cell cell = playgroundCells[row][column];
        if (cell.isOccupied()) {
            registerPlayer(player);
        }
        cell.setPlayer(player);
        player.setCell(cell);
        registeredPlayers.add(player);
    }

    public void activate(){
        Thread t = new Thread(this);
        t.start();
        logger.log(Level.INFO, "Playground activated.");
    }

    public void evictPlayer(Player player) {
        registeredPlayers.remove(player);
        player.getCell().setPlayer(null);
    }

    public void start(){
        logger.log(Level.INFO, "Initializing players.");
        for (Player player : registeredPlayers) {
            player.ready();
        }
    }
    public final Cell getCell(int row, int column) {
        try {
            return playgroundCells[row][column];
        } catch (ArrayIndexOutOfBoundsException ex) {
            return null;
        }
    }

    public Set<Player> getRegisteredPlayers() {
        return registeredPlayers;
    }

    public void setReferee(Referee referee) {
        this.referee = referee;
    }

    public Referee getReferee() {
        return referee;
    }
}

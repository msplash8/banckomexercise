package banckom.test.exercise;

public interface IReferee {
    // Check if referee need to give a yellow card to a player.
    boolean checkPlayerMovementForFoul(Player player, Movement movement);
    // Eject player from the game after subsequent ejection.
    void ejectPlayerFromTheGame(Player player);
    // Return true if cell is not occupied by other player.
    boolean checkIfPlayerMoveIsAllowed(Player player, Movement movement);
}

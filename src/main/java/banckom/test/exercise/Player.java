package banckom.test.exercise;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Player implements IPlayer {
    private static final Logger logger = Logger.getLogger(Player.class.getName());
    // Encapsulation, we dont want to give access from outside to set yellow card count.
    private int yellowCardCount = 0;

    private int ejectionCount = 0;

    private Cell cell = null;

    private int id;
    // Self reference to use in anonymous classes
    private Player player = this;

    public Player() {
    }

    // Increases the count of yellow cards. Called by the referee.
    public void yellowCard() {
        yellowCardCount++;
        if( yellowCardCount > 1 )
            ejectionCount++;

        if (ejectionCount < 2) {
            askRefereeToPlayAgain();
        }
    }


    public final void askRefereeToPlayAgain() {
        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
        exec.schedule(new Runnable() {
            public void run() {
                Playground.getInstance().getReferee().returnPlayerToTheGame(player);
                //Discard yellow card count.
                player.yellowCardCount = 0;
            }
        }, 10, TimeUnit.SECONDS);

    }

    public boolean isNeedToBeEjected() {
        if (yellowCardCount == 2 || ejectionCount > 1) {
            return true;
        } else {
            return false;
        }
    }

    // Initializing executor which will generate movement every second.
    public void ready() {
        logger.log(Level.INFO, "Initializing player: {0}.", this);
        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
        exec.scheduleAtFixedRate(new Runnable() {
            public void run() {
                Playground.getInstance().requestMove(new PlayerMovement(player, generateMovement()));
            }
        }, 0, 1, TimeUnit.SECONDS);
    }

    private Movement generateMovement() {
        // TODO: logic for generating movement
        return null;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public int getYellowCardCount() {
        return yellowCardCount;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Player{id=" + id + '}';
    }
}

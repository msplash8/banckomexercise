package banckom.test.exercise;

public class PlayerMovement {

    private Player player = null;
    private Movement movement = null;

    public PlayerMovement(Player player, Movement movement) {
        this.player = player;
        this.movement = movement;
    }

    public Player getPlayer() {
        return player;
    }

    public Movement getMovement() {
        return movement;
    }

}

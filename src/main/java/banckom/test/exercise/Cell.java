package banckom.test.exercise;

// Each cell is 1 meter.
public class Cell {
    private Player player = null;
    private Playground playground = null;
    private int row;
    private int column;

    public Cell(int row, int column, Playground playground) {
        this.row = row;
        this.column = column;
        this.playground = playground;
    }

    public Cell getLeftCell() {
        return playground.getCell(row, column -1);
    }

    public Cell getTopCell() {
        return playground.getCell(row -1, column);
    }

    public Cell getRightCell() {
        return playground.getCell(row, column +1);
    }

    public Cell getBottomCell() {
        return playground.getCell(row +1, column);
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isOccupied(){
        return (player == null) ? false: true;
    }

    public Cell getCellInDirection(Movement direction){
        Cell cell = null;

        if( Movement.LEFT == direction){
            cell = getLeftCell();
        }
        if( Movement.TOP == direction){
            cell = getTopCell();
        }
        if( Movement.RIGHT == direction){
            cell = getRightCell();
        }
        if( Movement.BOTTOM == direction){
            cell = getBottomCell();
        }

        return cell;
    }

    @Override
    public String toString() {
        return "Row: " + row + ", Column: " + column + ", Player: " + player;
    }
}
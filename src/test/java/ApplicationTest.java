import banckom.test.exercise.Player;
import banckom.test.exercise.Playground;
import banckom.test.exercise.Referee;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ApplicationTest {
    static Playground playground;
    @BeforeClass
    public static void init() {
        playground = Playground.getInstance();
        Referee referee = Referee.getInstance(playground);
        playground.setReferee(referee);
        for (int i = 0; i < 10; i++) {
            Player player = new Player();
            player.setId(i);
            playground.registerPlayer(player);
        }
        playground.activate();
        playground.start();
    }
    @Test
    public void testYellowCard() {
        Player player = playground.getRegisteredPlayers().iterator().next();
        player.yellowCard();
        Assert.assertEquals(1, player.getYellowCardCount());
    }

    // TODO: cover all functionality with unit tests :)

}

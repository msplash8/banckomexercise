package banckom.test.exercise;

// Application launcher
public class Main {
    private static final int NUMBER_OF_PLAYERS = 10;

    public static void main(String[] args) {
        Playground playground = Playground.getInstance();
        Referee referee = Referee.getInstance(playground);
        playground.setReferee(referee);
        for (int i = 0; i < NUMBER_OF_PLAYERS; i++) {
            Player player = new Player();
            player.setId(i);
            playground.registerPlayer(player);
        }
        playground.activate();
        playground.start();
    }
}

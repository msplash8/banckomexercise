package banckom.test.exercise;

public interface IPlayer {
    // Increases the yellow card number for player. Called by the referee.
    void yellowCard();
    // Method to check whether player should be ejected from the game after yellow card.
    boolean isNeedToBeEjected();

}
